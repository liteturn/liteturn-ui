# liteturn-ui

This is the frontend javascript application for Liteturn. This is hosted on S3 as a static site and uses cloudfront as a CDN. This allows us to use a free SSL from AWS.

## Installation

* `git clone <repository-url>` this repository
* `cd liteturn-ui`

## Running / Development

* `python -m SimpleHTTPServer 4200` (or any web server)
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Deploying

2/22/2017 - Currently files are just copied to the app.liteturn.com S3 bucket. Cloudfront will also need to be invalidated so that the new files propagate. 
