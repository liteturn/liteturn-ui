$(document).ready(function() {
  
  var lock = new Auth0Lock(AUTH0_CLIENT_ID, AUTH0_DOMAIN, {
    auth: {
      params: { scope: 'openid email app_metadata' } //Details: https://auth0.com/docs/scopes
    },
    theme: {
      logo: 'https://app.liteturn.com/images/L-logo-small.png',
      primaryColor: '#02cb7b'
    },
    languageDictionary: {
      title: "Liteturn"
    },
    closable: false
  });

  $('#btn-logout').click(function(e) {
    e.preventDefault();
    logout();
  })


  lock.on("authenticated", function(authResult) {
    lock.getProfile(authResult.idToken, function (err, profile) {
      if (err) {
        // Remove expired token (if any)
        localStorage.removeItem('id_token');
        // Remove expired profile (if any)
        localStorage.removeItem('profile');
        return alert('There was an error getting the profile: ' + err.message);
      } else {
        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('profile', JSON.stringify(profile));
        route();
      }
    });
  });

  var tokenExpired = function(token){
    var decoded = jwt_decode(token);
    if (decoded.exp < Date.now() / 1000) {
      logout();
      return true;
    }else{
      return false;
    }
  }

  var isAdmin = function(profile) {
    if (profile &&
        profile.app_metadata &&
        profile.app_metadata.roles &&
        profile.app_metadata.roles.indexOf('admin') > -1) {
      return true;
    } else {
       return false;
    }
  };

  var isUser = function(profile) {
    if (profile &&
        profile.app_metadata &&
        profile.app_metadata.roles &&
        profile.app_metadata.roles.indexOf('user') > -1) {
      return true;
    } else {
       return false;
    }
  };

  var route = function() {
    var id_token = localStorage.getItem('id_token');
    var current_location = window.location.pathname;
    
    if (undefined != id_token) {
        var expiredToken = tokenExpired(id_token);
        var profile = JSON.parse(localStorage.getItem('profile'));
        page.base('/#');
        page('/', index);
        page('/reports', reports);
        page('/reports/:reportid/configure', configureReport);
        page('/reports/:reportid/view/:instanceid', viewReport);
        page('/report-history', reportHistory);
        page('/connections', connections);
        page('/connections/new', newConnection);
        page('/connections/:connectionid', viewConnection);
        page('/oauth2callback', handleCallback);       
        page();
        function index(ctx) {
            // Need to handle the callback from the google oauth service. This is checking if the scope if being returned in the callback. 
            if (ctx.querystring.indexOf("code") >= 0){
              handleCallback(ctx.querystring);
            }else {
              page.redirect('/report-history');
            }
        }
        function reports() {
            var active = "reports";
            loadNavTemplate(profile,active);
            loadReportsTemplate(profile);
        }
        function reportHistory() {
            var active = "reports";
            loadNavTemplate(profile,active);
            loadReportHistoryTemplate(profile);
        }
        function configureReport(ctx) {
            var active = "reports";
            loadNavTemplate(profile,active);
            loadConfigureReportTemplate(ctx,profile);
        }
        function viewReport(ctx) {
            var active = "reports";
            loadNavTemplate(profile,active);
            loadViewReportTemplate(ctx);
        }
        function connections() {
            var active = "connections";
            loadNavTemplate(profile,active);
            loadConnectionsTemplate(profile);
        }
        function newConnection() {
            var active = "connections";
            loadNavTemplate(profile,active);
            loadNewConnectionTemplate(profile);
        }
        function viewConnection(ctx) {
            var active = "connections";
            loadNavTemplate(profile,active);
            loadViewConnectionTemplate(ctx,profile);
        }
        function handleCallback(querystring) {
            exchangeAuthCode(querystring);
            page.redirect('/connections/new');
        }
    } else { // user is not logged in.
        // Call logout just to be sure our local session is cleaned up.
        if ("/" != current_location) {
          logout();
        }
        // Show a login dialog.
        lock.show();
    }
  };

  route();

});

var logout = function() {
  localStorage.removeItem('id_token');
  localStorage.removeItem('profile');
  window.location.href = "/";
};

var getTokenObject = function() {
  // Gets a token object form local storage (after a successful login) to be sent in the AJAX calls to the API. 
  // This is sent to the AWS JS SDK as an additional param
  var header = {}
  if (localStorage.getItem('id_token')) {
      header = {'headers': {'Authorization': 'Bearer ' + localStorage.getItem('id_token')}};
  }
  return header; 
};

var loadNavTemplate = function(profile,active) {
  $.ajax({
     url: 'templates/nav.hbs',
     async: false, 

     error: function() {
        $('#info').html('<p>Error loading template</p>');
     },
     success: function(data) {
        var template = Handlebars.compile(data);
        var html = template(profile);
        $('.nav').html(html);

     },
     type: 'GET'
  });
  switch(active) {
    case "reports":
      $("#reports").removeClass("active");
      $("#reports").addClass("active");
      break;
    case "connections":
      $("#connections").removeClass("active");
      $("#connections").addClass("active");
      break;
  }
  $('.ui.dropdown').dropdown('refresh');
  $('.nav').on("click", "#btn-logout", function() {
    logout()
  });
};

var loadReportHistoryTemplate = function(profile) {
  var apiClient = apigClientFactory.newClient();
  var report_history_list;
  var additionalParams = {"queryParams": {"accountId":profile.app_metadata.account_id}} //If I want to send query params that are not defined in the API gateway, I have to send them as additonal params when using the SDK. 
  jQuery.extend(additionalParams, getTokenObject());
  $('#main-container').html('<div class="ui segment container very padded center aligned clearing"><div class="ui active inverted dimmer"><div class="ui text loader">Loading</div></div><p></p></div>')
  apiClient.reportHistoryGet({},{},additionalParams).then(result => {
     report_history_list = result.data;
      $.ajax({
         url: 'templates/report-history.hbs',

         error: function() {
            $('#info').html('<p>Error loading reports</p>');
         },
         success: function(data) {
            var template = Handlebars.compile(data);
            var html = template(report_history_list);
            $('#main-container').html(html);
            $('#report-history-table').DataTable();
            //Format the search input
            $('#report-history-table_filter input').unwrap();
            $('#report-history-table_filter input').attr('placeholder', "Search...");
            $('#report-history-table_filter').contents().filter(function () {
                return this.nodeType === 3; // Text nodes only
            }).remove();
            $('#report-history-table_filter').addClass('icon');
            $('#report-history-table_filter').append('<i class="search icon"></i>');


         },
         type: 'GET'
      });
  });
};



var loadReportsTemplate = function(profile) {
  var apiClient = apigClientFactory.newClient();
  var report_list;
  var additionalParams = {"queryParams": {"accountId":profile.app_metadata.account_id}} //If I want to send query params that are not defined in the API gateway, I have to send them as additonal params when using the SDK. 
  jQuery.extend(additionalParams, getTokenObject());
  $('#main-container').html('<div class="ui segment container very padded center aligned clearing"><div class="ui active inverted dimmer"><div class="ui text loader">Loading</div></div><p></p></div>')
  apiClient.reportsGet({},{},additionalParams).then(result => {
  report_list = result.data;
    $.ajax({
       url: 'templates/reports.hbs',

       error: function() {
          $('#info').html('<p>Error loading reports</p>');
       },
       success: function(data) {
          var template = Handlebars.compile(data);
          var html = template(report_list);
          $('#main-container').html(html);
       },
       type: 'GET'
    });
  });
};

var loadConfigureReportTemplate = function(context,profile) {
  var apiClient = apigClientFactory.newClient();
  var report = context.params;
  var additionalParams = {"queryParams": {"accountId":profile.app_metadata.account_id}} //If I want to send query params that are not defined in the API gateway, I have to send them as additonal params when using the SDK. 
  jQuery.extend(additionalParams, getTokenObject());
  $('#main-container').html('<div class="ui segment container very padded center aligned clearing"><div class="ui active inverted dimmer"><div class="ui text loader">Loading</div></div><p></p></div>')
  apiClient.reportsReportIdGet({"report_id":report.reportid},{},additionalParams).then(result => {
  report_list = result.data;
    $.ajax({
       url: 'templates/configure-report.hbs',

       error: function() {
          $('#info').html('<p>Error loading reports</p>');
       },
       success: function(data) {
          var template = Handlebars.compile(data);
          var html = template(report_list);
          $('#main-container').html(html);
          //Get the available connections for this account and add them to the dropdown. 
          apiClient.connectionsGet({},{},additionalParams).then(result => {
            $.each(result.data['connections'], function(key, value) {   
                 $('#select-connection').append($("<option></option>").attr("value",value['connectionId']).text(value['name'])); 
            });
          });
          $('.ui.dropdown').dropdown('refresh');
          $('#select-connection').change(function() {
            $('#select-campaign').parent('div').removeClass('loading').addClass('loading');
            var connectionId = $( this ).val();
            //Get a list of campaigns for the selected connection and add them to the dropdown.
            apiClient.connectionsConnectionIdOptionGet({"connection_id":connectionId,"option":"orders"},{},additionalParams).then(result => {
              $.each(result.data['orders'], function(key, value) {   
                   $('#select-campaign').append($("<option></option>").attr("value",value['id']).text(value['name'])); 
              });
              $('#select-campaign').parent('div').removeClass('loading')
            });
          });
          $('#select-campaign').change(function() {
            $('#run-report').removeClass('disabled');
          });
          // I know this is hacky
          $('#run-report').click(function(e) {
            $('#run-report').removeClass('disabled').addClass('disabled');
            $('#run-report').removeClass('loading').addClass('loading');
            $('#run-report').submit(function(e){
              e.preventDefault();
              var formData = {
                'connectionId': $('#select-connection option:selected').val(),
                'connectionName': $('#select-connection option:selected').text(),
                'orderId': $('#select-campaign option:selected').val(),
                'orderName': $('#select-campaign option:selected').text(),
                'reportId': report_list['reports'][0]['reportId'],
                'reportName': report_list['reports'][0]['name'],
                'createdById': profile.user_id,
                'createdByName': profile.email
              };
              var additionalParams = {"queryParams": {"accountId":profile.app_metadata.account_id}}
              jQuery.extend(additionalParams, getTokenObject());
              //alert(formData['connectionName']+":"+formData['connectionId']+" | "+formData['orderName']+":"+formData['orderId']+" | "+formData['reportName']+":"+formData['reportId']+" | "+formData['createdByName']+":"+formData['createdById']);
              apiClient.reportHistoryPost({},formData,additionalParams).then(result => {
                if (typeof result.data['reportInstanceId'] != "undefined") {
                  window.location.href = "/#/reports/"+formData['reportId']+"/view/"+result.data['reportInstanceId'];
                } else {
                  alert("Error creating your report.");
                }
              });
            });
            $('#run-report').submit();
          });

       },
       type: 'GET'
    });

  });
};

var loadViewReportTemplate = function(ctx) {
  // Taking the context of the query which should have the report id and the report instance id.
  // Following convention here, the reportid should have a matching [reportid].hbs file in the templates/report-templates dir. 
  // $( ".ui.section.divider" ).after('<div class="ui icon compact message"><i class="setting loading icon"></i><div class="content"><div class="header">Your Report is Running...</div><p>We\'re pulling the data and creating your report. This should take about a minute.</p></div></div>');
  $('#main-container').html('<div class="ui segment container very padded center aligned clearing"><div class="ui active dimmer"><div class="ui text loader">Loading Report</div></div><p></p></div>')
  report = ctx.params; 
  var reportData = {"report": {"reportid": "1","id": "6f3843e9-5778-4976-88b8-8ac6543b7eb0","name": "Campaign Overview","campaign": "ACME_Hammers_SPRING_2017","created": "02/10/2017"}}
  $.ajax({
     url: 'templates/report-templates/'+report.reportid+'.hbs',

     error: function() {
        $('#info').html('<p>Error loading the report.</p>');
     },
     success: function(data) {
        var template = Handlebars.compile(data);
        var html = template(reportData);
        $('#main-container').html(html);
        // Load all of the report charts and tables from report.js
        loadLineItemPerformanceChart();
        loadLineItemPerformanceTable(); 
        loadLineItemByDateChart(); 
        loadLineItemByDayChart();   
     },
     type: 'GET'
  });
};

var loadConnectionsTemplate = function(profile) {
  var apiClient = apigClientFactory.newClient();
  var connection_list;
  var additionalParams = {"queryParams": {"accountId":profile.app_metadata.account_id}} //If I want to send query params that are not defined in the API gateway, I have to send them as additonal params when using the SDK. 
  jQuery.extend(additionalParams, getTokenObject());
  $('#main-container').html('<div class="ui segment container very padded center aligned clearing"><div class="ui active inverted dimmer"><div class="ui text loader">Loading</div></div><p></p></div>')
  apiClient.connectionsGet({},{},additionalParams).then(result => {
     connection_list = result.data;
      $.ajax({
         url: 'templates/connections.hbs',

         error: function() {
            $('#info').html('<p>Error loading reports</p>');
         },
         success: function(data) {
            var template = Handlebars.compile(data);
            var html = template(connection_list);
            $('#main-container').html(html);

         },
         type: 'GET'
      });
  });
};

var loadViewConnectionTemplate = function(context,profile) {
  var apiClient = apigClientFactory.newClient();
  var connection = context.params;
  var additionalParams = {"queryParams": {"accountId":profile.app_metadata.account_id}}  
  jQuery.extend(additionalParams, getTokenObject());
  $('#main-container').html('<div class="ui segment container very padded center aligned clearing"><div class="ui active inverted dimmer"><div class="ui text loader">Loading</div></div><p></p></div>')
  apiClient.connectionsConnectionIdGet({"connection_id":connection.connectionid},{},additionalParams).then(result => {
     connection_list = result.data;
      $.ajax({
         url: 'templates/view-connection.hbs',

         error: function() {
            $('#info').html('<p>Error loading connection</p>');
         },
         success: function(data) {
            var template = Handlebars.compile(data);
            var html = template(connection_list);
            $('#main-container').html(html);

            apiClient.connectionsConnectionIdOptionGet({"connection_id":connection.connectionid,"option":"networks"},{},additionalParams).then(result => {
              $.each(result.data['networks'], function(key, value) {   
                   $('#network-name').append($("<option></option>").attr("value",value['networkCode']).attr("data-displayname",value['displayName']).text(value['displayName']+" - "+value['networkCode'])); 
              });
            });

            // I know this is hacky
            $('#save-connection').click(function(e) {
              $('#edit-connection').submit(function(e){
                e.preventDefault();
                var formData = {
                  'connectionName': $('#connection-name').val(),
                  'networkName': $('#network-name :selected').data('displayname'),
                  'networkCode': $('#network-name').val()
                };
                var additionalParams = {"queryParams": {"accountId":profile.app_metadata.account_id}}
                jQuery.extend(additionalParams, getTokenObject());
                apiClient.connectionsConnectionIdPatch({"connection_id":connection.connectionid},formData,additionalParams).then(result => {
                  $('#save-connection').removeClass("disabled").addClass("disabled");
                });
              });
              $('#edit-connection').submit();
            });

         },
         type: 'GET'
      });
  });
};

var loadNewConnectionTemplate = function(context) {
  $.ajax({
     url: 'templates/new-connection.hbs',

     error: function() {
        $('#info').html('<p>Error loading reports</p>');
     },
     success: function(data) {
        var template = Handlebars.compile(data);
        var html = template(context);
        $('#main-container').html(html);
        $('#authDFPButton').click(function(e) {
          e.preventDefault();
          createOauthRequestDFP();
        });
        $('#authGAButton').click(function(e) {
          e.preventDefault();
          createOauthRequestGA();
        }); 
     },
     type: 'GET'
  });
};

