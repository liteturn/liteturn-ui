var client_id = "728269833024-djfvj4rqlsgg4b29og60esl97hgc8kv5.apps.googleusercontent.com";
var app_id = "XXXXXX-XXXXXXXXXX";
var currentUrlBase = document.location.origin;

var createOauthRequestDFP = function() {
  // Setup the URL for the request
  var url = "https://accounts.google.com/o/oauth2/v2/auth?";
  var scope = "scope="+encodeURIComponent("https://www.googleapis.com/auth/dfp");
  var options = "&access_type=offline&include_granted_scopes=true&state=state_parameter_passthrough_value&response_type=code";
  options +="&prompt=consent";
  var redirectUri = "&redirect_uri="+encodeURIComponent(currentUrlBase);
  var clientID = "&client_id="+client_id;
  //Temperarally set the connection request type
  localStorage.setItem('connection_request', "DFP");
  // Redirect the user to the google authorzize page. 
  var authorizeUrl = url.concat(scope,options,redirectUri,clientID);
  window.location.href = authorizeUrl; 
};

var createOauthRequestGA = function() {
  // Setup the URL for the request
  var url = "https://accounts.google.com/o/oauth2/v2/auth?";
  var scope = "scope="+encodeURIComponent("https://www.googleapis.com/auth/analytics.readonly");
  var options = "&access_type=offline&include_granted_scopes=true&state=state_parameter_passthrough_value&response_type=code";
  options +="&prompt=consent";
  var redirectUri = "&redirect_uri="+encodeURIComponent(currentUrlBase);
  var clientID = "&client_id="+client_id;
  //Temperarally set the connection request type
  localStorage.setItem('connection_request', "GA");
  // Redirect the user to the google authorzize page. 
  var authorizeUrl = url.concat(scope,options,redirectUri,clientID);
  window.location.href = authorizeUrl; 
};

var exchangeAuthCode = function(response){
  //Getting the callback query from the inital oauth flow. Here we will parse the query string and get the code value which we'll use to get the access / refresh tokens. 
  var responseData = response?JSON.parse('{"' + response.replace(/&/g, '","').replace(/=/g,'":"') + '"}',function(key, value) { return key===""?value:decodeURIComponent(value) }):{}
  var data = "code="+responseData.code+"&client_id="+client_id+"&client_secret="+app_id+"&redirect_uri="+encodeURIComponent(currentUrlBase)+"&grant_type=authorization_code"
  $.ajax({
      type: "POST",
      url: "https://www.googleapis.com/oauth2/v4/token",
      data: data,
      success: function(data) {
        if (localStorage.getItem('connection_request') == 'DFP'){
          createGoogleConnection(data,"DFP");
        }
        if (localStorage.getItem('connection_request') == 'GA'){
          createGoogleConnection(data,"GA");
        }
        localStorage.removeItem('connection_request');
      },
      error: function() {
        console.error("Error exchanging the auth code for a token.")
      },
      dataType: 'json'
    });
  };

var createGoogleConnection = function(data,provider){
    if (typeof data.refresh_token !== 'undefined'){
      var providerButtonId = '';
      if (provider == "DFP"){
        providerButtonId = '#authDFPButton';
      }else if (provider == "GA"){
        providerButtonId = '#authGAButton';
      }
      $(providerButtonId).addClass("loading");
      profile = JSON.parse(localStorage.getItem('profile'));
      var body = {"accountId":profile.app_metadata.account_id,"refreshToken":data.refresh_token,"provider":provider}
      var apiClient = apigClientFactory.newClient();
      apiClient.connectionsPost({},body,getTokenObject()).then(result => {
        connection_list = result.data; //I'm not sure what else needs to happen here. 
        $(providerButtonId).removeClass("labeled loading").addClass("circular").off();
        $(providerButtonId).html('<i class="checkmark icon large"></i>');
        $('#nextButton').removeClass("disabled");
      });
    }else {
      console.error("Error: There was no refresh token in the response. A connection with DFP can not be made.")
    }
};
