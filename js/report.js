var loadLineItemPerformanceChart = function() {
    var lineItemPerformanceChart = $("#lineItemPerformanceChart");
    var LIPC = new Chart(lineItemPerformanceChart, {
        width: 842,
        height: 300,
        type: 'bar',
        data: {
          labels: ["Weather_160x600","Homepage_300x250","Nationwide_300x250","Sports_160x600","Local Events_728x90"],
          datasets: [
            {
              type: 'scatter',
              label: 'CTR',
              showLine: false,
              data: [0.05,0.08,0.01,0.01,0.02],
              backgroundColor: "rgba(255,99,132,0.9)",
              borderColor: "rgba(255,99,132,1)",
              borderWidth: 1,
              yAxisID: "y-axis-1"
            },
            {
              type: 'bar',
              label: 'Impressions',
              data: [700164,690800,390876,300125,225340],
              backgroundColor: 'rgba(120, 205, 150, .8)',
              borderColor: 'rgba(160, 218, 204, 1)',
              borderWidth: 1,
              yAxisID: "y-axis-0",
            }
          ]
        },
        options: {
            tooltips: {
              mode: 'x-axis'
            },
            elements: {
              point: {
                hitRadius: 20,
              }
            },
            maintainAspectRatio: false,
            responsive: false,
            scales: {
                yAxes: [{
                    position: "left",
                    "id": "y-axis-0",
                    gridLines: {
                        display:false
                    }
                }, {
                    position: "right",
                    "id": "y-axis-1",
                    gridLines: {
                        display:false
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display:false
                    }
                }]
            }
        }
    });
};

var loadLineItemPerformanceTable = function() {
    // Get this table data from the generated json file. 
    var tableData = [
    {
      "Served Impressions": 772091,
      "Value Delivered": 15750.0,
      "Budget": 15750.0,
      "Rate": 20.4,
      "Goal Impressions": 772059,
      "Line item": "6917-ACME_Hammers 2016_HP_CLST-141539094",
      "Clicks": 414
    },
    {
      "Served Impressions": 419143,
      "Value Delivered": 5130.0,
      "Budget": 5140.8,
      "Rate": 12.24,
      "Goal Impressions": 420000,
      "Line item": "6918-ACME_Hammers 2016_SLP_block1-142673807",
      "Clicks": 55
    },
    {
      "Served Impressions": 728095,
      "Value Delivered": 4455.0,
      "Budget": 5140.8,
      "Rate": 6.12,
      "Goal Impressions": 840000,
      "Line item": "6919-ACME_Hammers 2016_SLP_block2/MR-141538682-141534382",
      "Clicks": 0
    },
    {
      "Served Impressions": 1336458,
      "Value Delivered": 20447.0,
      "Budget": 21037.5,
      "Rate": 15.3,
      "Goal Impressions": 1375000,
      "Line item": "6920-ACME_Hammers 2016_mSLP_block1-141539647",
      "Clicks": 1548
    },
    {
      "Served Impressions": 772069,
      "Value Delivered": 18902.0,
      "Budget": 18900.0,
      "Rate": 24.48,
      "Goal Impressions": 772059,
      "Line item": "6921-ACME_Hammers 2016_PLP_block1_1st-142294653",
      "Clicks": 388
    },
    {
      "Served Impressions": 739778,
      "Value Delivered": 9054.0,
      "Budget": 11599.99,
      "Rate": 12.24,
      "Goal Impressions": 947712,
      "Line item": "6922-ACME_Hammers 2016_PLP_block2_1st-141538886",
      "Clicks": 21
    },
    {
      "Served Impressions": 959331,
      "Value Delivered": 7043.0,
      "Budget": 9115.04,
      "Rate": 7.34,
      "Goal Impressions": 1241831,
      "Line item": "6923-ACME_Hammers 2016_PLP_block3/MR_1st-141540103-142293633",
      "Clicks": 14
    },
    {
      "Served Impressions": 514771,
      "Value Delivered": 10501.0,
      "Budget": 10500.0,
      "Rate": 20.4,
      "Goal Impressions": 514706,
      "Line item": "6929-ACME_Hammers 2016_mHP_block1-141540104",
      "Clicks": 244
    },
    {
      "Served Impressions": 1725564,
      "Value Delivered": 8802.0,
      "Budget": 8800.0,
      "Rate": 5.1,
      "Goal Impressions": 1725490,
      "Line item": "6930-ACME_Hammers 2016_mHP_block2-141539098",
      "Clicks": 253
    },
    {
      "Served Impressions": 686285,
      "Value Delivered": 10501.0,
      "Budget": 10500.01,
      "Rate": 15.3,
      "Goal Impressions": 686275,
      "Line item": "6931-ACME_Hammers 2016_aPLP & aPDP-141539514",
      "Clicks": 124
    },
    {
      "Served Impressions": 1527551,
      "Value Delivered": 9349.0,
      "Budget": 11900.0,
      "Rate": 6.12,
      "Goal Impressions": 1944445,
      "Line item": "6932-ACME_Hammers 2016_PDP_MR_CXT-141539479",
      "Clicks": 25
    },
    {
      "Served Impressions": 1535056,
      "Value Delivered": 18787.0,
      "Budget": 18899.99,
      "Rate": 12.24,
      "Goal Impressions": 1544117,
      "Line item": "6933-ACME_Hammers 2016_PDP_LB_CXT-142673091",
      "Clicks": 272
    },
    {
      "Served Impressions": 1482234,
      "Value Delivered": 9071.0,
      "Budget": 11600.0,
      "Rate": 6.12,
      "Goal Impressions": 1895425,
      "Line item": "6934-ACME_Hammers 2016_PLP_block3/MR_CXT-141539772-141536150",
      "Clicks": 51
    },
    {
      "Served Impressions": 1857210,
      "Value Delivered": 37886.0,
      "Budget": 37887.17,
      "Rate": 20.4,
      "Goal Impressions": 1857214,
      "Line item": "6935-ACME_Hammers 2016_PLP_block1_CXT-142673808",
      "Clicks": 4789
    },
    {
      "Served Impressions": 343143,
      "Value Delivered": 10498.0,
      "Budget": 10499.99,
      "Rate": 30.6,
      "Goal Impressions": 343137,
      "Line item": "6943-1_App: Homepage",
      "Clicks": 168
    },
    {
      "Served Impressions": 1029422,
      "Value Delivered": 8401.0,
      "Budget": 8400.0,
      "Rate": 8.16,
      "Goal Impressions": 1029412,
      "Line item": "6944-ACME_Hammers 2016_mPLP_block1-141539928",
      "Clicks": 851
    },
    {
      "Served Impressions": 772062,
      "Value Delivered": 31500.0,
      "Budget": 31500.01,
      "Rate": 40.8,
      "Goal Impressions": 772059,
      "Line item": "7849-ACME_Hammers 2016_HP Roadblock-141539478",
      "Clicks": 288
    }
  ]
    var html = `<table class="ui celled small compact table">\ 
      <thead><tr><th>Line Item</th>\
      <th>Goal Impressions</th>\
      <th>Served Impressions</th>\
      <th>Clicks</th>\
      <th>CTR</th>\
      <th>Budget</th>\
      <th>Value Delivered</th>\
      <th>Rate</th>\
      </tr></thead>\
      <tbody>`;
    for (var i=0; i < tableData.length; i++) {
      var row = "<tr><td>"+tableData[i]['Line item']+"</td><td>"+tableData[i]['Goal Impressions']+"</td><td>"+tableData[i]['Served Impressions']+"</td><td>"+tableData[i]['Clicks']+"</td><td>"+tableData[i]['CTR']+"</td><td>"+tableData[i]['Budget']+"</td><td>"+tableData[i]['Value Delivered']+"</td><td>"+tableData[i]['Rate']+"</td></tr>";
      html = html.concat(row);
    }
    html = html.concat("</table>");
    $('#lineItemPerformanceTable').html(html);
};

var loadLineItemByDateChart = function() {
    var lineItemByDateChart = $("#lineItemByDateChart");
    var LIBDC = new Chart(lineItemByDateChart, {
        width: 525,
        height: 330,
        type: 'bar',
        data: {
        labels: ["9/5/2016","9/6/2016","9/7/2016","9/8/2016","9/9/2016","9/10/2016","9/11/2016","9/12/2016","9/13/2016","9/14/2016","9/15/2016","9/16/2016","9/17/2016","9/18/2016","9/19/2016","9/20/2016","9/21/2016","9/22/2016","9/23/2016","9/24/2016","9/25/2016","9/26/2016","9/27/2016","9/28/2016","9/29/2016","9/30/2016"],
        datasets: [
            {
              type: 'line',
              label: 'CTR',
              showLine: true,
              fill: false,
              data: [0.04,0.035,0.025,0.048,0.049,0.09,0.05,0.055,0.02,0.03,0.05,0.051,0.06,0.035,0.048,0.049,0.04,0.03,0.04,0.05,0.042,0.065,0.055,0.07,0.091,0.05],
              backgroundColor: "rgba(255,99,132,0.9)",
              borderColor: "rgba(255,99,132,1)",
              borderWidth: 1,
              yAxisID: "y-axis-1",
              tension:0,
            },
            {
              type: 'bar',
              label: 'Impressions',
              data: [20000,145000,270000,85000,60000,55000,140000,100000,90000,85000,110000,150000,110000,100000,130000,75000,22000,40000,44000,60000,282000,58000,50000,49000,48000,40000],
              backgroundColor: 'rgba(120, 205, 150, .8)',
              borderColor: 'rgba(160, 218, 204, 1)',
              borderWidth: 1,
              yAxisID: "y-axis-0",
            }
        ]
        },
        options: {
            tooltips: {
              mode: 'x-axis'
            },
            elements: {
              point: {
                hitRadius: 20,
              }
            },
            maintainAspectRatio: false,
            responsive: false,
            scales: {
                yAxes: [{
                    position: "left",
                    "id": "y-axis-0",
                    ticks: {
                        suggestedMin: 0
                    },
                    gridLines: {
                        display:false
                    }
                  }, {
                    position: "right",
                    "id": "y-axis-1",
                    ticks: {
                        suggestedMin: 0
                    },
                    gridLines: {
                        display:false
                    }
                  }],
                xAxes: [{
                    gridLines: {
                        display:false
                    }
                }]
            }
        }
    });
};

var loadLineItemByDayChart = function() {
    var lineItemByDayChart = $("#lineItemByDayChart");
    var LIBDC = new Chart(lineItemByDayChart, {
        width: 525,
        height: 330,
        type: 'bar',
        data: {
              labels: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
              datasets: [
                {
                  type: 'line',
                  label: 'CTR',
                  showLine: true,
                  fill: false,
                  data: [0.04,0.055,0.033,0.03,0.05,0.046,0.058],
                  backgroundColor: "rgba(255,99,132,0.9)",
                  borderColor: "rgba(255,99,132,1)",
                  borderWidth: 1,
                  yAxisID: "y-axis-1",
                  tension:0,
                },
                {
                  type: 'bar',
                  label: 'Impressions',
                  data: [325000,250000,315000,560000,325000,275000,425000],
                  backgroundColor: 'rgba(120, 205, 150, .8)',
                  borderColor: 'rgba(160, 218, 204, 1)',
                  borderWidth: 1,
                  yAxisID: "y-axis-0",
                }
              ]
        },
        options: {
            tooltips: {
              mode: 'x-axis'
            },
            elements: {
              point: {
                hitRadius: 20,
              }
            },
            maintainAspectRatio: false,
            responsive: false,
            scales: {
                yAxes: [{
                    position: "left",
                    "id": "y-axis-0",
                    ticks: {
                        suggestedMin: 0
                    },
                    gridLines: {
                        display:false
                    }
                  }, {
                    position: "right",
                    "id": "y-axis-1",
                    ticks: {
                        suggestedMin: 0
                    },
                    gridLines: {
                        display:false
                    }
                  }],
                xAxes: [{
                    gridLines: {
                        display:false
                    }
                }]
            }
        }
    });
};
